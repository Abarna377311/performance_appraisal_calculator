package com.wipro.performance.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.wipro.performance.bean.EmployeeBean;
import com.wipro.performance.exception.InvalidADIDException;
import com.wipro.performance.exception.InvalidBUException;
import com.wipro.performance.exception.InvalidCurrentSalaryException;
import com.wipro.performance.exception.InvalidDOJException;

public class Service {
	public String validateData(EmployeeBean	ebean)
	{
		SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");
		Date d=new Date();
		String msg="";
		try
		{
		if((ebean.getADID().length()!=6)||(!ebean.getADID().matches("[a-zA-Z0-9]+")))
			{
			throw new InvalidADIDException();
			}	
		else if(!ebean.getBusinessUnit().equalsIgnoreCase("JAVA")&&!ebean.getBusinessUnit().equalsIgnoreCase("Oracle")&&!ebean.getBusinessUnit().equalsIgnoreCase("BigData"))
			{
			throw new InvalidBUException();
			}
		else if(ebean.getDateOfJoining().after(d)||(ebean.getDateOfJoining().getDate()==d.getDate()&&ebean.getDateOfJoining().getMonth()==d.getMonth()&&ebean.getDateOfJoining().getYear()==d.getYear()))
			{	
			throw new InvalidDOJException();
			}
		else if(ebean.getCurrentSalary()<=50000)
			{
			throw new InvalidCurrentSalaryException();
			}
		else if(ebean.getTotalAttendance()<=0||ebean.getTotalAttendance()>=200)
			{
			msg="Invalid Attendance";
			}
		else if(ebean.getManagerRating()<=0||ebean.getManagerRating()>=5)
			{
			msg="Invalid Rating";
			}
		else 
			msg="SUCCESS";
		}
		catch(InvalidADIDException i)
		{
			msg=i.toString();
		}
		catch(InvalidBUException i)
		{
			msg=i.toString();
		}
		catch(InvalidDOJException i)
		{
			msg="Invalid Date Of Joining";
		}
		catch(InvalidCurrentSalaryException i)
		{
			msg=i.toString();
		}
		System.out.println("The validation of the given employee details :"+msg);
		return msg;
	}
	public String computeAppraisal(EmployeeBean ebean)
	{
		float sal = 0,sal1 = 0;
		float final_sal;
		String msg = "",s=validateData(ebean);
		if(s.equalsIgnoreCase("SUCCESS"))
		{
			if(ebean.getTotalAttendance()>=0&&ebean.getTotalAttendance()<=100)
				sal=0;
			else if(ebean.getTotalAttendance()>=101&&ebean.getTotalAttendance()<=150)
				sal=(float)(0.05*(ebean.getCurrentSalary()));
			else if(ebean.getTotalAttendance()>=151&&ebean.getTotalAttendance()<=200)
				sal=(float) (0.06*ebean.getCurrentSalary());
			if(ebean.getManagerRating()==0)
				sal1=0;
			else if(ebean.getManagerRating()==1)
				sal1=(float)(0.06+ebean.getCurrentSalary());
			else if(ebean.getManagerRating()==2)
				sal1=(float)(0.07*ebean.getCurrentSalary());
			else if(ebean.getManagerRating()==3)
				sal1=(float)(0.08*ebean.getCurrentSalary());
			else if(ebean.getManagerRating()==4)
				sal1=(float)(0.09*ebean.getCurrentSalary());
			else if(ebean.getManagerRating()==5)
				sal1=(float)(0.10*ebean.getCurrentSalary());
			System.out.println("Increase in salary from attendance: "+sal+"\nIncrease in salary from manager rating :"+sal1);
			System.out.println("Previous salary is :"+ebean.getCurrentSalary());
			final_sal=sal+sal1+ebean.getCurrentSalary();
			msg=Float.toString(final_sal);
		}
		else
		{
			msg=s;
		}
		return msg;
	}
	public String getAppraisalDetails(EmployeeBean ebean)
	{
		String msg="",s;
		s=computeAppraisal(ebean);
		try
		{
		Float sal=Float.parseFloat(s);
		if(sal>ebean.getCurrentSalary())
		{
		ebean.setCurrentSalary(sal);
		msg=ebean.getADID()+" : "+ebean.getCurrentSalary();
		}
		}
		catch(Exception e)
		{
			msg=ebean.getADID()+" : "+s;
		}
		return msg;
	}
	}

