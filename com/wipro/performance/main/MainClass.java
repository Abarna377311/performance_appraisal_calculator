package com.wipro.performance.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.wipro.performance.bean.EmployeeBean;
import com.wipro.performance.entity.Service;

public class MainClass {

	public static void main(String args[]) throws ParseException, IOException
	{
		EmployeeBean eb=new EmployeeBean();
		Service s=new Service();
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the ADID : ");
		//ADID should be 6 digit alpha numeric
		String adid=br.readLine();
		eb.setADID(adid);
		System.out.println("Enter the Employee Name : ");
		String name=br.readLine();
		eb.setEmpName(name);
		//Business Unit should be JAVA / Oracle /BigData
		System.out.println("Enter the Business Unit : ");
		String bu=br.readLine();
		eb.setBusinessUnit(bu);
		//DOJ shouldn't be current date or Future Date
		System.out.println("Enter the Date Of Joning : ");
		String date=br.readLine();
		SimpleDateFormat sd=new SimpleDateFormat("dd/MM/yyyy");
		Date d=sd.parse(date);
		eb.setDateOfJoining(d);
		//Salary should be above 50000
		System.out.println("Enter the Current Salary : ");
		int sal=Integer.parseInt(br.readLine());
		eb.setCurrentSalary(sal);
		//Attendance should be between 0-200
		System.out.println("Enter the Total Attendance : ");
		int att=Integer.parseInt(br.readLine());
		eb.setTotalAttendance(att);
		//Manager Rating should be between 0-5
		System.out.println("Enter the Manager Rating : ");
		int mgr=Integer.parseInt(br.readLine());
		eb.setManagerRating(mgr);
		System.out.println();
		System.out.println("\nThe Appraisal details :\n\n"+s.getAppraisalDetails(eb));
	}
}
